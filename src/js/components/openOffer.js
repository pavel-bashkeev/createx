export const openOffer = () => {
  const offerItems = document.querySelectorAll('.offer-item');


  offerItems.forEach(item => {
    item.addEventListener('click', (event) => {
      let target = event.currentTarget;
      let offerText = target.querySelector('.offer-item__text');

      for (let offer of offerItems) {
        offer.classList.contains('open') ? offer.classList.remove('open') : '';
      }

      if (!event.target.classList.contains('offer-item__btn')) {
        return false
      }
      item.classList.add('open');
      for (let offer of offerItems) {
        offer.querySelector('.offer-item__text').style.maxHeight = null;
        if(offer.classList.contains('open')){
          offerText.style.maxHeight = offerText.scrollHeight + 'px'
        }
      }
    })
  })
}