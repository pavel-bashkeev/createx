// import Swiper from "swiper"

export const sliderPortfolioMain = () => {
  const sliderPortfolio = new Swiper('.portfolio-slider', {
    loop: true,
    slidesPerView: 1.2,
    spaceBetween: 30,
    centeredSlides: true,
    slideToClickedSlide: true,
    speed: 1500,
    slideShadows: true,
    // on: {
    //   init: () => {
    //     console.log('init slider');

    //     const activeSlide = document.querySelector('.swiper-slide-active');
    //     const nextSlide = activeSlide.nextElementSibling;
    //     const prevSlide = activeSlide.previousElementSibling;
    //     activeSlide.classList.add('slide-project__visible');
    //     nextSlide.classList.add('slide-project__visible');
    //     prevSlide.classList.add('slide-project__visible');
    //   },
    //  },
    navigation: {
      nextEl: '.portfolio-slide-next',
      prevEl: '.portfolio-slide-prev',
    },
    breakpoints: {
      576: {
        slidesPerView: 2,
        centeredSlides: false,
      },
      768: {
        slidesPerView: 3,
      }
    }
  })
  const sliderPortfolioBtnNext = () => {
    const slideBtn = document.querySelector('')
  }
}
export const sliderClientsReviews = () => {
  const sliderReviews = new Swiper('.clients-reviews__slider', {
    slidesPerView: 1,
    spaceBetween: 20,
    loop: true,
    speed: 1500,
    navigation: {
      nextEl: '.clients-reviews__slide-next',
      prevEl: '.clients-reviews__slide-prev'
    }
  })
}