///////////////////////////////////////////////////////////////////////////////////
//date from moment.js
///////////////////////////////////////////////////////////////////////////////////

export const workDate = () => {
    moment.locale(document.getElementsByTagName("html")[0].getAttribute("lang"));
    
    document.querySelectorAll('.js-current-date').forEach(node => {
      node.innerHTML = moment().format('DD.MM.YYYY');
    });
  
    document.querySelectorAll('.js-current-month').forEach(node => {
      node.innerHTML = moment().format('MMMM');
    });
  
    document.querySelectorAll('.js-current-year').forEach(node => {
      node.innerHTML = moment().format('YYYY');
    });
  
    document.querySelectorAll('.js-publish-date-notime').forEach(node => {
      node.innerHTML = moment().add(-6 ,'days').format('LL');
    });
    document.querySelectorAll('.day-1').forEach(node => {
      node.innerHTML = moment().add(-1 ,'days').format('DD.MM.YYYY');
    });
    document.querySelectorAll('.day-2').forEach(node => {
      node.innerHTML = moment().add(-2 ,'days').format('LL');
    });
    document.querySelectorAll('.day-3').forEach(node => {
      node.innerHTML = moment().add(-3 ,'days').format('DD.MM.YYYY');
    });
    document.querySelectorAll('.day-4').forEach(node => {
      node.innerHTML = moment().add(-4 ,'days').format('DD.MM.YYYY');
    });
    document.querySelectorAll('.day-5').forEach(node => {
      node.innerHTML = moment().add(-5 ,'days').format('DD.MM.YYYY');
    });
    document.querySelectorAll('.day-6').forEach(node => {
      node.innerHTML = moment().add(-6 ,'days').format('DD.MM.YYYY');
    });
}
///////////////////////////////////////////////////////////////////////////////////
//date from moment.js
///////////////////////////////////////////////////////////////////////////////////