export const scrollUp = () => {
  const scrollUpBtn = document.querySelector('.up-scroll__btn');
  const scrollUpInner = document.querySelector('.up-scroll');
  const flagScroll = document.querySelector('*[data-scroll]');
  flagScroll ? document.body.classList.add('scroll-smooth') : '';
  window.addEventListener('scroll', () => {
    window.pageYOffset > 700 ? scrollUpInner.classList.add('visible'): scrollUpInner.classList.remove('visible');
  })
  scrollUpBtn.addEventListener('click', () => {
   document.body.scrollIntoView({
     behavior: 'smooth',
     block: 'start',     
   })
  })
}