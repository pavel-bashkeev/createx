export const factsProgress = () => {
  const circles = document.querySelectorAll('.facts-item__circle');

  circles.forEach(item => {
    // Элементы круга
    const circleIcon = item.querySelector('.facts-item__circle-icon');
    let circleValueText = item.querySelector('.facts-item__circle-value');

  // Флаг процента в значениях
    let flagPercent = circleIcon.dataset.percentValue;
    // Получаем элемента прогресса
    const circleValueBlock = circleIcon.querySelector('.progress-circle');
    // Получаем значения фактические прогресса заполнения
    const circleValueProgress = parseInt(circleValueBlock.dataset.value);
    // Получаем значения полного заполнения прогресса
    const circleValueFull = parseInt(circleValueBlock.dataset.full);

    //Получаем радиус круга прогресса
    const radiusCircle = parseInt(circleValueBlock.getAttribute('r'));
    //Вычесляем длинну окружности
    const lenghtCircle = 2 * Math.PI * radiusCircle;


    // Получаем процент заполнения 
    const fillPercentage = Math.floor((100 * circleValueProgress) / circleValueFull);
    
    // Точка начала заолнения , но нужно еще повернуть на -90deg и сдвинуть по x на -100%
    circleValueBlock.setAttribute('stroke-dasharray', lenghtCircle);
    // Заполняем прогресс
    circleValueBlock.setAttribute('stroke-dashoffset', lenghtCircle - (lenghtCircle * fillPercentage / 100))
    // добовляем в значене знак % взависимости от flagPercent
    flagPercent == "true" ? circleValueText.innerHTML = `${circleValueProgress}%` : circleValueText.innerHTML = `${circleValueProgress}`;

  })
}