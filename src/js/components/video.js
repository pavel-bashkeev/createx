export const videoPlay = () => {
  const videoBtn = document.querySelector('.video-block__play');
  const videoValue = document.querySelector('.video-block__content');
  const videoBlock = document.querySelector('.video-block');

  videoBtn.addEventListener('click', () => {
    videoValue.play();
    videoBtn.classList.add('hide-btn');
    videoBlock.classList.add('hide-bg');
  })
  videoValue.addEventListener('click', ()=>{
    videoValue.pause();
    videoBtn.classList.remove('hide-btn');
    videoBlock.classList.remove('hide-bg');
  })
}