export const changeArticles = () => {
  const articlesMainPage = document.querySelectorAll('.news-item');

  articlesMainPage.forEach(item => {
    item.addEventListener('click', ()=>{
      if(!item.classList.contains('news-item--big')){
        if(item.classList.contains('news-item--topRight')){
          document.querySelector('.news-item--big').classList.add('news-item--topRight')
          item.classList.remove('news-item--topRight')
          item.classList.add('news-item--big')
          document.querySelector('.news-item--topRight').classList.remove('news-item--big')
        }
        if(item.classList.contains('news-item--bottomRight')){
          document.querySelector('.news-item--big').classList.add('news-item--bottomRight')
          item.classList.remove('news-item--bottomRight')
          item.classList.add('news-item--big')
          document.querySelector('.news-item--bottomRight').classList.remove('news-item--big')
        }
      }
    })
  })
}