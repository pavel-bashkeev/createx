import documentReady from './modules/documentReady.js';
import lazyImages from './modules/lazyImages.js';
import * as webpSupportFunctions from './modules/webpSupport.js';
import {
  sliderPortfolioMain,
  sliderClientsReviews
} from './components/sliders.js';
import {
  factsProgress
} from './components/factsProgress.js';
import {
  videoPlay
} from './components/video.js';
import {
  scrollUp
} from './components/scrollUp.js';
import { openOffer } from './components/openOffer.js';

documentReady(() => {
  console.log('ready');
  lazyImages();
  webpSupportFunctions.isWebp();
  document.querySelector('.portfolio-slider') ? sliderPortfolioMain() : '';
  document.querySelector('.clients-reviews__slider') ? sliderClientsReviews() : '';
  document.querySelector('.facts') ? factsProgress() : '';
  document.querySelector('.video-block') ? videoPlay() : '';
  document.querySelector('.offer') ? openOffer(): '';
  scrollUp();

})